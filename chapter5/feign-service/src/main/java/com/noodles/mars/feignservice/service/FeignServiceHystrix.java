package com.noodles.mars.feignservice.service;

import org.springframework.stereotype.Component;


@Component
public class FeignServiceHystrix implements FeignService {
    @Override
    public String hello(String name) {
        return String.format("Hello, %s! Access remote service fail!", name);
    }
}
