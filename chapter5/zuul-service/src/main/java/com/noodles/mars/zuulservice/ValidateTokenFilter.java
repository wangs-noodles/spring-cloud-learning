package com.noodles.mars.zuulservice;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


@Component
public class ValidateTokenFilter extends ZuulFilter {

    private final static String VALIDATE_TOKEN_FILTER_TYPE = "pre";

    private final static String TOKEN_PARAM_NAME = "token";

    private final Logger logger = LoggerFactory.getLogger(ValidateTokenFilter.class);

    @Override
    public String filterType() {
        return VALIDATE_TOKEN_FILTER_TYPE;
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();

        HttpServletRequest request = ctx.getRequest();
        logger.info("Request info: method {{}} url {{}}", request.getMethod(), request.getRequestURL().toString());

        String token = request.getParameter(TOKEN_PARAM_NAME);

        if(ObjectUtils.isEmpty(token)) {
            logger.warn("Token in request is empty.");
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);

            try {
                ctx.getResponse().getWriter().write("Token can not be empty!");
            } catch (IOException e) {
                return null;
            }
        }
        logger.info("Validate via...");
        return null;
    }
}
