package com.noodles.mars.feignservice.api;

import com.noodles.mars.feignservice.service.FeignService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeignController {

    /** Logger.*/
    private final Logger logger = LoggerFactory.getLogger(FeignController.class);
    private final FeignService feignService;

    @Autowired
    public FeignController(FeignService feignService) {
        this.feignService = feignService;
    }

    @GetMapping(value = "/hello")
    public String hello(@RequestParam("name") String name) {
        logger.info("Access from feign client.");

        return feignService.hello(name);
    }
}
