package com.noodles.mars.ribbonservice.api;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
public class HelloController {

    private final RestTemplate restTemplate;

    @Autowired
    public HelloController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @HystrixCommand(fallbackMethod = "helloError")
    @GetMapping("/hello")
    public String hello(@RequestParam("name") String name) {
        return restTemplate.getForObject("http://HELLO-EUREKA-CLIENT/hello?name=" + name, String.class);
    }

    public String helloError(String name) {
        return String.format("Hello, %s! Access remote service fail!", name);
    }
}
