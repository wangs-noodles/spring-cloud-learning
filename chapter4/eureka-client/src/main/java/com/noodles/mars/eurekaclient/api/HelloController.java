package com.noodles.mars.eurekaclient.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HelloController {

    @Value("${server.port}")
    int port;

    @GetMapping("/hello")
    public String hello(@RequestParam("name") String name) {
        return String.format("Hello, My name is %s, I'm from port: %d", name, port);
    }
}
