### 项目介绍

本人在自己的博客中发表了几篇`Spring Cloud`教学文章，仓库中的代码都是文章中的实战源码，想学习的朋友fork到本地研究！

### Spring Cloud

`Spring Cloud`为开发人员提供了快速构建分布式系统的工具（例如：配置管理，服务发现，断路器，智能路由，微代理，控制总线，一次性令牌，全局锁，领导选举，分布式会话，集群状态等等），开发人员可以用Spring Cloud快速搭建具有以上功能的应用程序。

### 项目说明

- chapter1: [Spring Cloud 教程一：Eureka](http://blog.hxpgxt.cn/2019/10/09/SpringCloud%E6%95%99%E7%A8%8B%E4%B8%80Eureka/)
- chapter2: [Spring Cloud教程二：Ribbon](http://blog.hxpgxt.cn/2019/10/10/SpringCloud%E6%95%99%E7%A8%8B%E4%BA%8CRibbon/)
- chapter3: [Spring Cloud教程三：Feign](http://blog.hxpgxt.cn/2019/10/14/SpringCloud%E6%95%99%E7%A8%8B%E4%B8%89Feign/)
- chapter4: [Spring Cloud教程四：Hystrix](http://blog.hxpgxt.cn/2019/10/14/SpringCloud%E6%95%99%E7%A8%8B%E5%9B%9BHystrix/)
- chapter5: [Spring Cloud教程五：Zuul](http://blog.hxpgxt.cn/2019/10/15/SpringCloud%E6%95%99%E7%A8%8B%E4%BA%94Zuul/)
- chapter6: [Spring Cloud教程六：Config](http://blog.hxpgxt.cn/2019/10/16/SpringCloud%E6%95%99%E7%A8%8B%E5%85%ADConfig/)


### 个人信息

想要了解更多，关注我的公众号：**JAVA九点半课堂**， 技术文章第一时间送达。

个人博客：[http://blog.hxpgxt.cn](http://blog.hxpgxt.cn)