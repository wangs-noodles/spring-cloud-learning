package com.noodles.mars.backendservice;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BackendController {

    @GetMapping("/backend-api")
    public String backendApi() {
        return "Hello, My name is Mars!";
    }
}
