package com.noodles.mars.frontendservice;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class FrontendController {

    private final RestTemplate restTemplate;

    @Autowired
    public FrontendController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/frontend-api")
    public String frontendApi() {
        String backendUrl = "http://localhost:8090/backend-api";
        return restTemplate.getForObject(backendUrl, String.class);
    }
}
