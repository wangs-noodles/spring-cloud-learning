package com.noodles.mars.configclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HelloController {

    @Value("${name}")
    private String name;

    @GetMapping("/hello")
    public String hello() {
        return String.format("Hello, My name is %s!", name);
    }
}
