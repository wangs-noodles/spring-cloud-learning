package com.noodles.mars.feignservice.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(value = "hello-eureka-client")
public interface FeignService {

    @GetMapping(value = "/hello")
    String hello(@RequestParam(value = "name") String name);
}
